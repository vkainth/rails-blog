# Rails Blog

A simple blog in Ruby on Rails.

## Introduction

The Rails Blog is a microblogging application that implements the basic features
of a blog. It uses paperclip for image uploading and sorcery for authentication.

## Installation

- Clone the repo using ssh or https

- In the cloned directory, run `bundle install`

- Run `bin/rails server` to start the server

- Navigate to `https://localhost:3000` to view the local application

## Production

- You can use heroku to publish the application

- Follow
  [these steps](https://www.theodinproject.com/courses/web-development-101/lessons/your-first-rails-application?ref=lnav)
